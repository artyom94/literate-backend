const Joi = require('@hapi/joi');

const schemas = {
   signUp: {
       body: Joi.object({
           firstName: Joi.string()
               .alphanum()
               .max(30)
               .required(),
           lastName: Joi.string()
               .alphanum()
               .max(30)
               .required(),
           email: Joi.string()
                .email().
                required(),
           password: Joi.string()
               .min(3)
               .max(15)
               .required(),
           //  repeat_password: Joi.ref('password'),
       }),
       query: {},
       params: {},
   },
    signIn: {
        body: Joi.object({
            email: Joi.string().email().max(256).required().label('Email'),
            password: Joi.string().min(3).max(15).required().label('Password'),
        }),
        query: {},
        params: {},
    },
};

module.exports = schemas;