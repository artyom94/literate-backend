'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    const records = [];
    for (let i = 0; i <= 100; i++) {
      records.push(
          {
            createrUuid: 'e8b05858-bfe0-4936-b2c5-31a1fab6133b',
            title: Math.random() + '___',
            description: Math.random() + '======',
            createdAt: new Date(),
            updatedAt: new Date()
          }
      )
    }
    return queryInterface.bulkInsert('Lessons', records, {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('Lessons', null, {});
  }
};
