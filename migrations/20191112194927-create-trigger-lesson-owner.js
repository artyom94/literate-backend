'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(
      'CREATE TRIGGER tr_Lesson_Owner AFTER INSERT ON `Lessons` FOR EACH ROW' +
                  ' BEGIN' +
                      ' INSERT INTO TutorLessons (`tutorUuid`, `lessonId`, `createdAt`, `updatedAt`)' + 
                      ' VALUES (NEW.createrUuid, NEW.id, now(), now());' +
                  'END;'
    )
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.query(
      'DROP TRIGGER IF EXISTS tr_Lesson_Owner;'
    )
  }
};