'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('UserExamAnswers', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      userUuid: {
        type: Sequelize.UUID,
        references: {
          model: 'Users', // name of Target model
          key: 'uuid', // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
      },
      examId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Exams', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
      },
      choiceId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Exams', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
      },
      isRight: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
        defaultValue: 0,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('UserExamAnswers');
  }
};