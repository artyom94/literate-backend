'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Lessons', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      /* TODO correction: creater to creator */
      createrUuid: {
        type: Sequelize.UUID,
        references: {
          model: 'Users', // name of Source model
          key: 'uuid',
        },
      },
      title: {
        type: Sequelize.STRING,
        allowNull: false
      },
      description: {
        type: Sequelize.STRING,
        allowNull: false
      },
      srcUrl: {
        type: Sequelize.STRING,
        allowNull: true
      },
      status:  {
        type: Sequelize.ENUM('draft','unpublished','published'),
        allowNull: false
      },
      /* TODO correction default value */
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
    });
    queryInterface.sequelize.query(
    `CREATE TRIGGER create_config AFTER INSERT ON users
    FOR EACH ROW
    BEGIN
    insert into configs (UserId) values(new.id);
    END;`)
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Lessons');
  }
};