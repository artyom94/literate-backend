module.exports = (app) => {
   const settings = app.locals.settings;
   const SDB      = settings.SDB;
   const { Exam } = SDB;

   const create = async (examData, currentUser) => {
      const {choices, ...question } = examData
      
      let transaction;    

      try {
         transaction = await SDB.sequelize.transaction();
         
         const Question = await Exam.create({
            ...question,
            'createrUuid': currentUser
         }, {transaction});

         choices.map(choice => choice.questionId = Question.id)

         await Exam.bulkCreate(choices,  { transaction });
         
         await transaction.commit();

      } catch (err) {
         if (err) await transaction.rollback();
      }
   }

   const update = async (id, examData) => {  
      const {choices, ...question } = examData
      
      let transaction;    

      try {
         transaction = await SDB.sequelize.transaction();
         
         const Question = await Exam.update({
            ...question,
         }, {returning: true, where: {id: id}, transaction});

         if (choices) {
            choices.map(choice => choice.questionId = Question.id)  
            await Exam.bulkCreate(choices,  { updateOnDuplicate: ['choice', 'isRight'],  transaction });
         }
         
         await transaction.commit();

      } catch (err) {
         if (err) await transaction.rollback();
      }

   }
   const destroy = async (id) => {  
      return await Exam.destroy({
         where: {
            'id': id
         }
     })

   }

   const getAll = async () => {
      return await Exam.findAll({
         where: {
            questionId: null,
         },
         attributes: {
            exclude: ['choice', 'isRight', 'questionId']
         },
         include: [
            {
               all: true,
               nested: true,
               attributes: ['id', 'choice', 'isRight']
            }
         ],
      })
   }
  
   const getById = async (id) => {      
      return await Exam.findOne({
         where: {
            id: id,
            questionId: null,
         },
         attributes: {
            exclude: ['choice', 'isRight', 'questionId']
         },
         include: [
            {
               all: true,
               nested: true,
               attributes: ['id', 'choice', 'isRight']
            }
         ],
      })
   }

  return {
     getAll,
     getById,
     create,
     update,
     destroy
  }

} 