module.exports = (app) => {
   const settings = app.locals.settings;
   const SDB      = settings.SDB;
   const { User, Tutor, Learner, Lesson, TutorLesson }   = SDB;

   const create = async (lessonData, currentUser) => {
      return await Lesson.create({
         ...lessonData,
         'createrUuid': currentUser
       });
   }

   const update = async (id, lessonData) => {  
      return await Lesson.update({
         ...lessonData,
         }, {
            where: {
               'id': id
            }
      });

   }
   const destroy = async (id) => {  
      return await Lesson.destroy({
         where: {
            'id': id
         }
     })

   }

   const getAll = async (paginationConfig) => {
      return await Lesson.findAndCountAll({
         ...paginationConfig,
         include: [
            {
               all: true,
               nested: true
            }
         ],
      })
   }
  
   const getById = async (id) => {      
      return await Lesson.findOne({
         where: {
            id: id,
         },
         include: [
            {
               all: true,
               nested: true
            }
         ],
      })
   }

  return {
     getAll,
     getById,
     create,
     update,
     destroy
  }

} 