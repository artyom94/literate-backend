module.exports = (app) => {
   const settings = app.locals.settings;
   const SDB      = settings.SDB;
   const { User, UserInfo }   = SDB;

   const getAll = async () => {
      return await User.findAll({
         include: [
            {
               all: true,
               nested: true
            }
          ],
      });
   }
  
   const getById = async (uuid) => {    
      return User.findOne({
         where: {
            uuid : uuid
         },
         include: [
            {
               model: UserInfo,
               attributes: {
                  exclude: ['userUuid'] 
               },
            }, 
         ]
      });
  }

  return {
     getAll,
     getById
  }

} 