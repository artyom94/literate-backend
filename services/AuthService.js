module.exports = (app) => {
   const settings = app.locals.settings;
   const SDB      = settings.SDB;
   const config   = settings.config;
   const jwt      = require('jsonwebtoken');
   const bcrypt   = require('bcryptjs');
   const {User, UserInfo}   = SDB;
   const passport = require('passport');

   const authenticate = async (req, res) => {
      passport.authenticate('local', {session: false}, (err, user, info) => {
         if (err || !user) {
             return res.status(400).json({
                 message: info ? info.message : 'Login failed',
                 user   : user
             });
         }
         req.login(user, {session: false}, (err) => {
            
            if (err) {
               res.send(err);
            }

            const { password, ...userWithoutHash } = user.toJSON();
            const token = jwt.sign(user.toJSON(), config.secret);

            res.cookie('jwt',token);
            return res.json({userWithoutHash});
         });
      }) (req, res);
   };

   const create = async (userParam) => {
      // validate
      if (await User.findOne({where : { email: userParam.email } })) {
         throw 'Email "' + userParam.email + '" is already taken';
      }

      const { email, password, status , ...userInfo } = userParam;
      const userFields = {
         email,
         // hash password
         password: bcrypt.hashSync(userParam.password, 10),
         status
      };

      let transaction;    

      try {
         transaction = await SDB.sequelize.transaction();
         
         const user = await User.create(userFields, {transaction});
         userInfo.userUuid = user.uuid;
         
         await UserInfo.create(userInfo,  { transaction });

         await transaction.commit();

      } catch (err) {
         console.log(err);
         
         if (err) await transaction.rollback();
      }
  };

  return {
     create,
     authenticate
  }
};