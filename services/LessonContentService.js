module.exports = (app) => {
   const settings = app.locals.settings;
   const SDB      = settings.SDB;
   const { Lesson, LessonContent }   = SDB;

   const create = async (lessonId, contentData) => {
      return await LessonContent.create({
         ...contentData,
         lessonId
       });
   }

   const update = async (id, contentData) => {  
      return await LessonContent.update({
         ...contentData,
         }, {
            where: {
               'id': id
            }
      });

   }
   const destroy = async (id) => {  
      return await LessonContent.destroy({
         where: {
            'id': id
         }
     })

   }

   const getAll = async (paginationConfig) => {
      return await LessonContent.findAndCountAll({
         ...paginationConfig,
         // include: [
         //    {
         //       all: true,
         //       nested: true
         //    }
         // ],
      })
   }
  
   const getById = async (id) => {      
      return await LessonContent.findOne({
         where: {
            id: id,
         },
         // include: [
         //    {
         //       all: true,
         //       nested: true
         //    }
         // ],
      })
   }

  return {
     getAll,
     getById,
     create,
     update,
     destroy
  }

} 