const express      = require('express');
const dotenv       = require('dotenv');
dotenv.config();
const path         = require('path');
const logger       = require('morgan');
const env          = process.env.NODE_ENV || 'development';
const config       = require('./config/config')[env];
const cookieParser = require('cookie-parser');
const Router       = express.Router;
const SequelizeObj = require(`${__dirname}/models`)
const errorHandler = require('./_helpers/errorHandler');
const paginate     = require('express-paginate');

const app = express();

app.set('env', process.env.NODE_ENV);
app.set('config',config);
app.set('basePath',__dirname);
app.set('Router', Router);
app.set('SDB', SequelizeObj);
app.set('paginate', paginate);

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());

app.use('/', require('./routes/index')(app));

app.use(errorHandler);

//default no route response
app.use(function(req, res, next) {
   res.status(404).send('Sorry cant find that!');
});


module.exports = app;
