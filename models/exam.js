'use strict';
module.exports = (sequelize, DataTypes) => {
  const Exam = sequelize.define('Exam', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    createrUuid: {
      type: DataTypes.UUID,
      references: 'Users', // <<< Note, its table's name, not object name
      referencesKey: 'uuid'
    },
    lessonId: {
      type: DataTypes.INTEGER,
      references: 'Lessons', // <<< Note, its table's name, not object name
      referencesKey: 'id'
    },
    question: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    isActive: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: 1,
    },
    questionId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: 'Exams', // <<< Note, its table's name, not object name
      referencesKey: 'id'
    },
    choice: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    isRight: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
    },
  }, {
    indexes: [
      {
          unique: false,
          fields: ['questionId']
      }
  ]
  });
  Exam.associate = function(models) {
    Exam.hasMany(Exam, {foreignKey: "questionId", as: 'choices' });
    Exam.hasMany(models.UserExamAnswer, {foreignKey: "id", as: 'examAnswers' });
  };
  return Exam;
};