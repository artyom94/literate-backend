'use strict';
module.exports = (sequelize, DataTypes) => {
  const LearnerLessons = sequelize.define('LearnerLesson', {
    learnerUuid: {
      type: DataTypes.UUID,
      references: 'Learners', // <<< Note, its table's name, not object name
      referencesKey: 'uuid'
    },
    lessonId: {
      type: DataTypes.INTEGER,
      references: 'Lessons', // <<< Note, its table's name, not object name
      referencesKey: 'id'
    },
  }, {});
  LearnerLessons.associate = function(models) {
    // associations can be defined here
  };
  return LearnerLessons;
};