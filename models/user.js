'use strict';
module.exports = (sequelize, DataTypes) => {
  const uuidv4 = require('uuid/v4');

  const User = sequelize.define('User', {
    uuid: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: () => uuidv4()
    },
    email: {
      type: DataTypes.STRING(50),
      allowNull: false,
      unique: true
    },
    password: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    status: {
      type: DataTypes.ENUM('active','blocked','deleted'),
      allowNull: true,
      defaultValue: 'active'
    },    
  }, {
    tableName   : 'Users',
  });

  User.prototype.toJSON =  function () {
    var values = Object.assign({}, this.get());
    
    delete values.password;
    return values;
  }

  User.associate = function(models) {
    User.hasOne(models.UserInfo, {foreignKey: "userUuid"});
    User.hasMany(models.Exam, {foreignKey: "createrUuid"});
    User.hasMany(models.UserExamAnswer, {foreignKey: "userUuid"});
    User.belongsToMany(
      models.Lesson, 
      {
        through: models.LearnerLesson,
        foreignKey: 'learnerUuid',
        otherKey: 'lessonId',
      }
    );
    User.belongsToMany(
      models.Lesson, 
      {
        through: models.TutorLesson,
        foreignKey: 'tutorUuid',
        otherKey: 'lessonId'
      }
    );
  };
  return User;
};