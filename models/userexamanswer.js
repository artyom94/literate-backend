'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserExamAnswer = sequelize.define('UserExamAnswer', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    userUuid: {
      type: DataTypes.UUID,
      references: 'Users', // <<< Note, its table's name, not object name
      referencesKey: 'uuid'
    },
    examId: {
      type: DataTypes.INTEGER,
        references: {
          model: 'Exams', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
    },
    choiceId: {
      type: DataTypes.INTEGER,
        references: {
          model: 'Exams', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
    },
    isRight: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
  }, {
    tableName   : 'UserExamAnswer'
  });
  UserExamAnswer.associate = function(models) {
    // associations can be defined here
  };
  return UserExamAnswer;
};