'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserInfo = sequelize.define('UserInfo', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    firstName: {
      type: DataTypes.STRING(25),
      allowNull: false
    },
    lastName: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    userUuid: {
      type: DataTypes.UUID,
      references: 'Users', // <<< Note, its table's name, not object name
      referencesKey: 'uuid'
    } 
  }, {
    tableName   : 'UsersInfo',
  });
  UserInfo.associate = function(models) {
    // associations can be defined here
  };
  return UserInfo;
};