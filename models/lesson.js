'use strict';
module.exports = (sequelize, DataTypes) => {
  const Lesson = sequelize.define('Lesson', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    title: {
      type: DataTypes.STRING(25),
      allowNull: false
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true
    }, 
    srcUrl: {
      type: DataTypes.STRING,
      allowNull: true
    },
    createrUuid: {
      type: DataTypes.UUID,
      references: 'Tutors', // <<< Note, its table's name, not object name
      referencesKey: 'uuid',
    }
  }, {
    tableName   : 'Lessons'
  });
  Lesson.associate = function(models) {
    // associations can be defined here
    Lesson.hasMany(models.Exam, {foreignKey: "lessonId"});
    Lesson.hasMany(models.LessonContent, {foreignKey: "lessonId"});
    Lesson.belongsToMany(
      models.User, 
      {
        through: models.TutorLesson,
        foreignKey: 'lessonId',
        otherKey: 'tutorUuid'
      }
    )

    Lesson.belongsToMany(
      models.User, 
      {
        through: models.LearnerLesson,
        foreignKey: 'lessonId',
        otherKey: 'learnerUuid'
      }
    )
  };

  return Lesson;
};