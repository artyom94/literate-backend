'use strict';
module.exports = (sequelize, DataTypes) => {
  const LessonContent = sequelize.define('LessonContent', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    lessonId: {
      type: DataTypes.INTEGER,
      references: 'Lessons', // <<< Note, its table's name, not object name
      referencesKey: 'id'
    },
    order: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
  }, {
    tableName   : 'LessonContents'
  });
  LessonContent.associate = function(models) {
    LessonContent.belongsTo(
      models.Lesson, 
      {
        foreignKey: 'lessonId',
      }
    )
  };
  return LessonContent;
};