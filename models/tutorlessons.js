'use strict';
module.exports = (sequelize, DataTypes) => {
  const TutorLessons = sequelize.define('TutorLesson', {
    tutorUuid: {
      type: DataTypes.UUID,
      references: 'Tutors', // <<< Note, its table's name, not object name
      referencesKey: 'uuid'
    },
    lessonId: {
      type: DataTypes.INTEGER,
      references: 'Lessons', // <<< Note, its table's name, not object name
      referencesKey: 'id'
    },
  }, {});
  TutorLessons.associate = function(models) {
    // associations can be defined here
  };
  return TutorLessons;
};