module.exports = (app) => { 
  
   const settings = app.locals.settings;
   const Router   = settings.Router();
   const passport = require('passport');
   const authController = require(`${settings.basePath}/Controllers/authController` )(app);
   const validation = require(`${settings.basePath}/_helpers/validationJoi`);
   const schema = require(`${settings.basePath}/validations/AuthSchema`);
 
   const { register, authenticate, logout, getUserInfo } = authController;
 
   /* auth routes */
   Router.post('/login', validation(schema.signIn), authenticate);
   Router.post('/register', validation(schema.signUp) ,register);
   Router.get('/logout', logout);
   Router.get('/me', passport.authenticate('jwt', {session: false}), getUserInfo);
 
   return Router;
};