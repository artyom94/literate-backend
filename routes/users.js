module.exports = (app) => { 
  const settings = app.locals.settings;
  const Router   = settings.Router();
  const userController = require(`${settings.basePath}/Controllers/userController` )(app);

  const { create, show, update, destroy, get } = userController;

  /* users routes */
  Router.get('/',      get);
  Router.post('/',     create)
  Router.get('/:uuid', show);
  Router.put('/',      update);
  Router.delete('/',   destroy);

  return Router;
   
}