module.exports = (app) => { 
  const settings = app.locals.settings;
  const Router   = settings.Router();
  const passport = require('passport');
  const paginate = settings.paginate;

  require(`${settings.basePath}/_helpers/passportJwt`)(app);
  
  /* pagination */
  app.all(function(req, res, next) {
    // set default or minimum is 10
    if (req.query.limit <= 10) req.query.limit = 10;
    next();
  });
  app.use(paginate.middleware(10, 50));

  /* GET users listing. */
  Router.get('/', function(req, res, next) {
    res.send('HomePage');
  });

  app.use('/auth', require('./auth')(app));
  app.use('/users', passport.authenticate('jwt', {session: false}), require('./users')(app));
  app.use('/lessons', passport.authenticate('jwt', {session: false}), require('./lesson')(app));
  app.use('/exam', passport.authenticate('jwt', {session: false}), require('./exam')(app));
  
  return Router

}
  