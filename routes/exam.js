module.exports = (app) => { 
   const settings = app.locals.settings;
   const Router   = settings.Router();
   const examController = require(`${settings.basePath}/Controllers/examController` )(app);
 
   const { create, show, update, destroy, get } = examController;

   /* examens routes */
   Router.post('/',      create);
   Router.get( '/',      get);
   Router.get('/:id',    show);
   Router.put('/:id',    update);
   Router.delete('/:id', destroy);
 
   return Router;   
}