module.exports = (app) => { 
   const settings = app.locals.settings;
   const Router   = settings.Router();
   const lessonController = require(`${settings.basePath}/Controllers/lessonController` )(app);
   const lessonContentController = require(`${settings.basePath}/Controllers/lessonContentController` )(app);
 
   const { create, show, update, destroy, get } = lessonController;
   const { create: createContent, show: showContent, update: updateContent, destroy: destroyContent, get: getContent } = lessonContentController;

   /* lessons routes */
   Router.post('/',     create)
   Router.get('/',      get);
   Router.get('/:id',   show);
   Router.put('/:id',   update);
   Router.delete('/:id',   destroy);

   /* lesson content routes */
   Router.post('/:lessonId/content',     createContent)
   Router.get('/:lessonId/content',      getContent);
   Router.get('/:lessonId/content/:id',   showContent);
   Router.put('/:lessonId/content/:id',   updateContent);
   Router.delete('/:lessonId/content/:id',   destroyContent);


   return Router;
}