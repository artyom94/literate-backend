module.exports = (app) => {
   const settings = app.locals.settings;
   const lessonContentService = require(`${settings.basePath}/services/LessonContentService`)(app);
   const paginate = settings.paginate; 

   return {     
      get(req, res, next) {
         const paginationConfig = {
            limit: req.query.limit,
            offset: req.skip,
         };

         lessonContentService.getAll(paginationConfig)
            .then(content => {
               const itemCount = content.count;
               const pageCount = Math.ceil(content.count / req.query.limit);
               const result = {
                  pageCount,
                  itemCount,
                  lessonsContent: content.rows,
                  pages : paginate.getArrayPages(req)(3, pageCount, req.query.page),
               }
               res.json(result)
            })
            .catch(err => next(err));
      },

      create(req, res, next) {
         const contentData  = req.body;
         const lessonId    = req.params.lessonId;
         
         lessonContentService.create(lessonId, contentData)
            .then(lessons => res.json(lessons))
            .catch(err => next(err));
      },

      show (req, res, next) {
         const id = req.params.id;
         
         lessonContentService.getById(id)
            .then( content => {
               res.json(content);
            } )
            .catch( err => next(err) )
      },

      update(req, res, next) {
         const id = req.params.id;
         const contentData  = req.body;
         
         lessonContentService.update(id, contentData)
            .then(() => res.status(204).send())
            .catch(err => next(err));
      },

      destroy(req, res, next) {
         const id = req.params.id;
         
         lessonContentService.destroy(id)
            .then(lesson => res.json(lesson))
            .catch(err => next(err));
      }
   }

}