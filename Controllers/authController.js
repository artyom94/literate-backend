module.exports = (app) => {
   const settings    = app.locals.settings;
   const AuthService = require(`${settings.basePath}/services/AuthService`)(app);

   return {
      authenticate(req, res, next) {
         return AuthService.authenticate(req, res);
      },

      register(req, res, next) {
         AuthService.create(req.body)
            .then(() => {
               return AuthService.authenticate(req, res);
            })
            .catch(err => next(err));
      },

      getUserInfo(req, res, next) {
         const user = req.user.toJSON();
         res.json(user);
      },

      logout(req, res, next) {
         req.logout();
         res.cookie('jwt', {expires: Date.now()});
         res.send('logout')
      }
      
   }
};