module.exports = (app) => {
   const settings = app.locals.settings;
   const lessonService = require(`${settings.basePath}/services/LessonService`)(app) 
   const paginate = settings.paginate; 

   return {     
      get(req, res, next) {
         const paginationConfig = {
            limit: req.query.limit,
            offset: req.skip,
         };

         lessonService.getAll(paginationConfig)
            .then(lessons => {
               const itemCount = lessons.count;
               const pageCount = Math.ceil(lessons.count / req.query.limit);
               const result = {
                  pageCount,
                  itemCount,
                  lessons: lessons.rows,
                  pages : paginate.getArrayPages(req)(3, pageCount, req.query.page),
               }
               res.json(result)
            })
            .catch(err => next(err));
      },

      create(req, res, next) {
         const currentUser = req.user.uuid;
         const lessonData  = req.body;
         
         lessonService.create(lessonData, currentUser)
            .then(lessons => res.json(lessons))
            .catch(err => next(err));
      },

      show (req, res, next) {
         const id = req.params.id;
         
         lessonService.getById(id)
            .then( lesson => {
               res.json(lesson);
            } )
            .catch( err => next(err) )
      },

      update(req, res, next) {
         const id = req.params.id;
         const lessonData  = req.body;
         
         lessonService.update(id, lessonData)
            .then(updatedLessons => res.json(updatedLessons))
            .catch(err => next(err));
      },

      destroy(req, res, next) {
         const id = req.params.id;
         
         lessonService.destroy(id)
            .then(lesson => res.json(lesson))
            .catch(err => next(err));
      }
   }

}