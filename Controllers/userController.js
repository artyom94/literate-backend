module.exports = (app) => {
   const settings    = app.locals.settings;
   const userService = require(`${settings.basePath}/services/UserService`)(app) 

   return {
      get(req, res, next) {
         userService.getAll()
            .then(users => res.json(users))
            .catch(err => next(err));
      },
      
      create(req, res, next) {
         res.send('show')
      },

      show(req, res, next) {
         const uuid = req.params.uuid;
         
         userService.getById(uuid)
            .then( user => {
               res.send({ user });
            } )
            .catch( err => next(err) )
      },

      update(req, res, next) {
         res.send('update')
      },

      destroy(req, res, next) {
         res.send('destroy')
      }
   }

}