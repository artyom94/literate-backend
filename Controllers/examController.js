module.exports = (app) => {
   const settings    = app.locals.settings;
   const examService = require(`${settings.basePath}/services/ExamService`)(app) 

   return {     
      get(req, res, next) {
         examService.getAll()
            .then(exams => res.json(exams))
            .catch(err => next(err));
      },

      create(req, res, next) {
         const currentUser = req.user.uuid;
         const examData  = req.body;

         examService.create(examData, currentUser)
            .then(() => res.status(201).send('created'))
            .catch(err => next(err));
      },

      show (req, res, next) {
         const id = req.params.id;
         
         examService.getById(id)
            .then( exam => {
               res.json(exam);
            } )
            .catch( err => next(err) )
      },

      update(req, res, next) {
         const id = req.params.id;
         const examData  = req.body;
         
         examService.update(id, examData)
            .then(() => res.status(204))
            .catch(err => next(err));
      },

      destroy(req, res, next) {
         const id = req.params.id;
         
         examService.destroy(id)
            .then(exam => res.json(exam))
            .catch(err => next(err));
      }
   }

}