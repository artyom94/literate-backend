module.exports = (app) => {    
    const settings = app.locals.settings;
    const SDB      = settings.SDB;
    const {User}   = SDB;
    const config   = settings.config;
    const userService = require(__dirname + '/../services/UserService.js')(app);

    const passport    = require('passport');
    const passportJWT = require("passport-jwt");    
    const LocalStrategy = require('passport-local').Strategy;
    const JWTStrategy   = passportJWT.Strategy;
    const bcrypt   = require('bcryptjs');

    const cookieExtractor = (req) => {
        let token = null;
        if (req && req.cookies) token = req.cookies['jwt'];
        return token;
    };

    passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },  (email, password, done) => {
        
            return User.findOne({where : { 'email' : email } })
                    .then(user => {

                        if (!user || !bcrypt.compareSync(password, user.password)) {
                            return done(null, false, {message: 'Incorrect email or password.'});
                        }
                        
                        if (user && bcrypt.compareSync(password, user.password)) {
                            return done(null, user, {
                                message: 'Logged In Successfully'
                            });
                        }
                        
                    })
                    .catch(err => {
                        return done(err);
                    });
        }
    
    ));
            
    passport.use(new JWTStrategy({
        jwtFromRequest: cookieExtractor,
        secretOrKey   : config.secret
    },  async (jwtPayload, done) => {
            const user = await userService.getById(jwtPayload.uuid);
            
            if (!user) {
                return done(null, false);
            }
            
            done(null, user);
        }
    ))

}